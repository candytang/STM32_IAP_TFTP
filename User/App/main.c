/*******************************************************************************
* Copyright (C), 2016-2019
*
* 文件名: main.c
* 内容简述: STM32F4x7 boot by tftp 主函数
*
* 文件历史:
* 版本号    日期      作者      说明
* v0.1    2016.12.31  candy    创建文件
* v0.2    2017.03.29  candy     修改框架
*******************************************************************************/

#include "stm32f4x7_eth.h"
#include "netconf.h"
#include "main.h"
#include "tftpserver.h"
#include "stm32f4xx_pwr.h"


/*******************************************************************************
*                           私有类型定义
*******************************************************************************/
typedef  void (*pFunction)(void);

/*******************************************************************************
*                           私有宏定义
*******************************************************************************/
#define SYSTEMTICK_PERIOD_MS  10  /* 时间基准的值，设置为 10ms */


/*******************************************************************************
*                           变量定义
*******************************************************************************/
__IO uint32_t LocalTime = 0;      /* 用于自减时间基准的变量 */
uint32_t timingdelay;             /* 延时变量 */
pFunction Jump_To_Application;    /* 应用程序跳转指针函数 */
uint32_t JumpAddress;             /* 应用程序跳转地址 */

/*******************************************************************************
*                           私有函数声明
*******************************************************************************/
static uint8_t IAP_IO_Detect(void);
static void LedFlash(uint32_t nCount);

extern uint32_t IPaddress;
extern uint32_t Netmask;
extern uint32_t Gateway;

/*******************************************************************************
*  函数名： main
*  输  入： 无
*  输  出： 无
*  调  用： 无
*  功能说明： 程序入口函数
*******************************************************************************/
int main(void)
{  
  
  /* 检测IAP引脚状态为非下载状态，且RTC备份寄存器的值未被置为0xA5A5。此时则不进入IAP状态 */
  if ((IAP_IO_Detect() != 1) && (RTC_ReadBackupRegister(RTC_BKP_DR1) != 0xA5A5))
  { 
    
    /* 如果RAM中的静态地址是有效的应用程序，则跳转到应用程序 */
    if (((*(__IO uint32_t*)USER_FLASH_FIRST_PAGE_ADDRESS) & 0x2FFE0000 ) == 0x20000000)
    {
       __set_PRIMASK(1);
      /* 计算用户应用程序入口地址 */
      JumpAddress = *(__IO uint32_t*) (USER_FLASH_FIRST_PAGE_ADDRESS + 4);
      Jump_To_Application = (pFunction) JumpAddress;
      
      /* 初始化用户堆栈指针 */
      __set_MSP(*(__IO uint32_t*) USER_FLASH_FIRST_PAGE_ADDRESS);
      
      /* 跳转到用户应用程序 */
      Jump_To_Application();
    }
    else
    {
      /* 错误，则进入死循环 */
      while(1)
      {
        LedFlash(100);
      };
    }
  }
  /* 进入IAP模式 */
  else
  {
    if (RTC_ReadBackupRegister(RTC_BKP_DR1) == 0xA5A5)
    {
        IPaddress = RTC_ReadBackupRegister(RTC_BKP_DR2);
        Netmask = RTC_ReadBackupRegister(RTC_BKP_DR3);
        Gateway = RTC_ReadBackupRegister(RTC_BKP_DR4);
    }
      
    /* 使能电源管理时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    
    PWR_BackupAccessCmd(ENABLE);
    /* 配置RTC配置寄存器的值为开始更新的状态值 */
    RTC_WriteBackupRegister(RTC_BKP_DR1, 0x0001);
    PWR_BackupAccessCmd(DISABLE);
    
    /* 配置以太网控制器参数 */ 
    ETH_BSP_Config();  

    /* 初始化Lwip堆栈信息 */
    LwIP_Init();

    /* 初始化TFTP服务器 */
    IAP_tftpd_init();
    
    while (1)
    {
      /* 判断以太网是否接收到数据包 */
      if (ETH_CheckFrameReceived())
      { 
        /* 处理接收到的网络数据包 */
        LwIP_Pkt_Handle();
      }
      /* Lwip时间间隔函数处理 */
      LwIP_Periodic_Handle(LocalTime);
      LedFlash(200);
    }
  }
}

/*******************************************************************************
*  函数名： LedFlash
*  输  入： nCount：延时的输入参数值
*  输  出： 无
*  调  用： main函数
*  功能说明： 用于设置LED闪烁的时间间隔
*******************************************************************************/
void LedFlash(uint32_t nCount)
{
  /* led时间间隔闪烁 */
  if ((LocalTime % nCount) == 0)
  {
    GPIO_ToggleBits(GPIOB, GPIO_Pin_5);
  }
}

/*******************************************************************************
*  函数名： Delay
*  输  入： nCount：延时的输入参数值
*  输  出： 无
*  调  用： main函数
*  功能说明： 用于设置阻塞延时的函数
*******************************************************************************/
void Delay(uint32_t nCount)
{
  /* 通过当前时间，设置延时时间 */
  timingdelay = LocalTime + nCount;  

  /* 等待延时完成 */  
  while(timingdelay > LocalTime);
}

/*******************************************************************************
*  函数名： Time_Update
*  输  入： 无
*  输  出： 无
*  调  用： SysTick_Handler
*  功能说明： 更新时间参数
*******************************************************************************/
void Time_Update(void)
{
  LocalTime += SYSTEMTICK_PERIOD_MS;
}

/*******************************************************************************
*  函数名： IAP_IO_Detect
*  输  入： 无
*  输  出： IAP检测的引脚状态
*  调  用： main函数
*  功能说明： 检测IAP引脚状态
*******************************************************************************/
static uint8_t IAP_IO_Detect(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
  GPIO_InitTypeDef  GPIO_Struct;    /* GPIO类型 */
  uint8_t InputState = 0;

  /* 配置系统节拍时钟 */
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

  /* 配置系统节拍时钟为每10ms产生一次中断 */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);  
    
  /* GPIOB时钟配置 */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

  /* 上拉输入模式 */
  GPIO_Struct.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_Struct.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;

  GPIO_Struct.GPIO_Pin   = GPIO_Pin_1;
  GPIO_Init(GPIOB, &GPIO_Struct);
  
  /* 上拉输出模式 */
  GPIO_Struct.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_Struct.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;

  GPIO_Struct.GPIO_Pin   = GPIO_Pin_5;
  GPIO_Init(GPIOB, &GPIO_Struct);

  /* 延时100ms */
  Delay(10);
    
  if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0)
  {
    /* 延时20ms */
    Delay(2);
    if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) == 0)
    {
      InputState = 1;
    }
  }
  
  return InputState;
}


#ifdef  USE_FULL_ASSERT
/*******************************************************************************
*  函数名： assert_failed
*  输  入： file：文件名；line：行号
*  输  出： 无
*  调  用： main函数
*  功能说明： 断言错误函数
*******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* 无限循环 */
  while (1)
  {
  
  }
}
#endif


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
